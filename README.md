PRTG Custom Sensors for monitoring Raise3D printers.
===========================================

More details of this project are outlined in this [blog article on the Paessler website.](https://blog.paessler.com/3d-printer-monitoring-with-prtg)


This project contains all the files necessary to monitor Raise3D N2 series 3d printers with PRTG Custom Script sensors [PRTG EXE/Script Advanced Sensor](https://www.paessler.com/manuals/prtg/exe_script_advanced_sensor).

Four separate scripts are included:

Raise3D_All.ps1 - provides all the metrics from the other scripts in single sensor.

Raise3D_System.ps1 - monitors system specific values (staus, bed temp, etc).

Raise3D_Extruder.ps1 - monitors extruder metrics (temp, feed rate).

Raise3D_Job.ps1 - Provides metrics on the current print job (duration, layer, etc).

Download Instructions
=========================
 [A zip file containing all the files in the installation package can be downloaded directly using this link](https://gitlab.com/PRTG/Sensor-Scripts/Raise3d/-/jobs/artifacts/master/download?job=PRTGDistZip)
.

The all-in-1 overview sensor for the Raise3D Printer.
![Image of Raise3D Sensor Channels](./Images/Raise3D_All.png)

A sensor showing the status of one of the extruders.
![Image of Raise3D Extruder Channels](./Images/Raise3D_Extruder.png)

A sensor showing the status of the current print job.
![Image of Raise3D Print Job Channels](./Images/Raise3D_Job.png)


Device Tree showing separate sensors grouped logically.
![Image of Raise3D Device Group](./Images/Raise3D_Group.png)


The separate sensors simplify the creation of maps (dashboards) like this one:
![Image of Raise3D Device Map](./Images/Raise3D_Map.png)

Installation Instructions
=========================

Please refer to INSTALL.md

