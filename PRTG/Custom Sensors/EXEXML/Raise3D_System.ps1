﻿# ___ ___ _____ ___
#| _ \ _ \_   _/ __|
#|  _/   / | || (_ |
#|_| |_|_\ |_| \___|
#    NETWORK MONITOR
#
#-------------------
# Description      This sensor will show the status of a Raise3d N2 Series 3d printer and current print job
#
#-------------------
# Requirements
#
# - Needs Get-Stringhash Powershell module, written by Spiceworks community member cduff.
# - Available here - https://community.spiceworks.com/scripts/show/2881-get-stringhash-gets-the-hash-of-an-string
#
# - Password:  The text MYAPIPASSWORD on line 46 must be replaced with the API password set on the printer.
# - IP Address: The IP address 192.168.100.1 on line 49 must be replaced with the IP address of the printer
# 
#-------------------
# Introduction
#
# This script retrieves status and print job information from Raise3d N2 series 3d printers, using the API functionality introduced
# with printer firmware 1.1.6-rev1 and RaiseTouch 1.1.0 released in October 2018. 
# For instructions on enabling the API, see the release notes at https://www.raise3d.com/blogs/news/raisetouch-1-1-0_release-notes  
#
# Each API call to the printer must include an access token which is calculated using SHA1 & MD5 hashes of the API password and
# the current time in Unix / POSIX / Epoch format. Each API call returns a JSON list containing the data for the corresponding
# printer component (system, extruders, job, etc). Some string returns (for example job status) are mapped to numeric values that
# can be translated into statuses in PRTG, using lookups. API returns are formatted as XML which is then processed by a PRTG
# EXE/Script Advanced Sensor.
#
#-------------------
# Version History
# 
# Version  Date        Notes
# 1.0      15/11/2018  Initial Release
#
# ------------------
# (c) 2018 Simon Bell | Paessler AG
# ------------------

# script parameters
Param (
        [string]$APIPass = $null,
        [string]$DeviceIP = $null, 
        [Int]$DevicePort = 10800, 
    [int]$debuglevel = 0
)

########################################################################################
# PreCondition checks
########################################################################################
# Check for required parameters
if (-not $APIPass) {
                return @"
<prtg>
  <error>1</error>
  <text>Required parameter not specified: please provide APIPass </text>
</prtg>
"@
}

if (-not $DeviceIP) {
                return @"
<prtg>
  <error>1</error>
  <text>Required parameter not specified: please provide DeviceIP</text>
</prtg>
"@
}
#URL of the IP address of the printer
$Printer = "http://" + $DeviceIP +":" + $DevicePort 

#Load hash calculation module
Import-Module get-stringhash.ps1

#Convert current time to unix time (milliseconds)
$Epoch=[Math]::Floor([decimal](Get-Date(Get-Date).ToUniversalTime()-uformat "%s"))

#Create SHA1 Hash for the password and timestamp strings
$Hash_SHA = Get-StringHash -Strings "password=$APIPass&timestamp=$Epoch" -Algorithm "SHA1" -ToLower

#create MD5 hash for the SHA1 string
$Hash_MD5 = Get-StringHash -Strings $Hash_SHA -Algorithm "MD5" -ToLower

#Pass MD5 Hash and timestamp to the printer to retrieve a valid token
$UriToken = $Printer+"/v1/login?sign=$Hash_md5&timestamp=$Epoch"
$ResposeToken = Invoke-RestMethod -uri $UriToken -Method Get

#Request returns a JSON list
$ListToken = $ResposeToken.data

#extract the token value as a variable
$AccessToken = $ListToken.token

#Start retrieving status data from the printer

#Get System Data
$UriSystem = $Printer+"/v1/printer/system?token=$AccessToken"
$ResponseSystem = Invoke-RestMethod -uri $UriSystem -Method Get
#Request returns a JSON list
$ListSystem = $ResponseSystem.data
#Round battery voltage to 2 decimal places
$BattVolt = [Math]::Round($ListSystem.battery,2)
#Convert storage KB to MB and round down
$StorageKB = ($ListSystem.storage_available / 1000)
$Storage = [Math]::Round($StorageKB,0)

#Get Camera Status
$UriCamera = $Printer+"/v1/printer/camera?token=$AccessToken"
$ResponseCamera = Invoke-RestMethod -uri $Uricamera -Method Get
#Request returns a JSON list
$ListCamera = $ResponseCamera.data
#Convert Camera status to numeric value so PRTG Lookups can be used  
if ($ListCamera.is_camera_connected -eq "true") {$CameraConnected = 1}
elseif ($ListCamera.is_camera_connected -eq "false") {$CameraConnected = 0}

#Get Running Status
$UriRunning = $Printer+"/v1/printer/runningstatus?token=$AccessToken"
$ResponseRunning = Invoke-RestMethod -uri $UriRunning -Method Get
#Request returns a JSON list
$ListRunning = $ResponseRunning.data

#Convert Running status to numeric value so PRTG Lookups can be used  
if ($ListRunning.running_status -match "idle") {$RunStatus = 0}
elseif ($ListRunning.running_status -match "running") {$RunStatus = 1}
elseif ($ListRunning.running_status -match "paused") {$RunStatus = 2}
elseif ($ListRunning.running_status -match "completed") {$RunStatus = 3}
elseif ($ListRunning.running_status -match "busy") {$RunStatus = 4}
elseif ($ListRunning.running_status -match "error") {$RunStatus = 5}


#Get Basic Information
$UriBasicInfo = $Printer+"/v1/printer/basic?token=$AccessToken"
$ResponseBasicInfo = Invoke-RestMethod -uri $UriBasicInfo -Method Get
#Request returns a JSON list
$ListBasicInfo = $ResponseBasicInfo.data

#Create results table for PRTG EXE/Script Advanced Sensor to process

Write-Host 
"<prtg>"
     "<result>"
        "<channel>6. Battery Voltage</channel>"
        "<value>"+$BattVolt+"</value>"
        #"<value>"+$ListSystem.battery+"</value>"
        "<unit>Custom</unit>"
        "<customunit>Volts</customunit>"
        "<float>1</float>"
        "<decimalmode>2</decimalmode>"
    "</result>"

    "<result>"
        "<channel>7. Extruder Count</channel>"
        "<value>"+$ListSystem.nozzies_num+"</value>"
    "</result>"
    
    "<result>"
        "<channel>8. Free Storage</channel>"
        #"<value>"+$ListSystem.storage_available+"</value>"
        "<value>"+$Storage+"</value>"
        "<unit>Custom</unit>"
        "<customunit>MB</customunit>"
    "</result>"

    "<result>"
        "<channel>9. Camera Connected</channel>"
        "<value>"+$CameraConnected+"</value>"
        "<unit>Custom</unit>"
        "<float>0</float>"
    "</result>"

    "<result>"
        "<channel>1. Printer Status</channel>"
        "<value>"+$RunStatus+"</value>"
    "</result>"

    "<result>"
        "<channel>2. Bed Temp Curr</channel>"
        "<value>"+$ListBasicInfo.heatbed_cur_temp+"</value>"
        "<unit>Custom</unit>"
        "<customunit>&#xb0;C</customunit>"
    "</result>"
    
    "<result>"
        "<channel>3. Bed Temp Tgt</channel>"
        "<value>"+$ListBasicInfo.heatbed_tar_temp+"</value>"
        "<unit>Custom</unit>"
        "<customunit>&#xb0;C</customunit>"
    "</result>"    

    "<result>"
        "<channel>4. Fan Speed Curr</channel>"
        "<value>"+$ListBasicInfo.fan_cur_speed+"</value>"
        "<unit>Custom</unit>"
        "<customunit>%</customunit>"
    "</result>"

    "<result>"
        "<channel>5. Fan Speed Tgt</channel>"
        "<value>"+$ListBasicInfo.fan_tar_speed+"</value>"
        "<unit>Custom</unit>"
        "<customunit>%</customunit>"
    "</result>"

"</prtg>"