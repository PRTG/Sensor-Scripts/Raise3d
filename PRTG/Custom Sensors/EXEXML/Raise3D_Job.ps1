# ___ ___ _____ ___
#| _ \ _ \_   _/ __|
#|  _/   / | || (_ |
#|_| |_|_\ |_| \___|
#    NETWORK MONITOR
#
#-------------------
# Name             Raise3D_Job.ps1
# Description      This sensor will show the status of a Raise3d N2 Series 3d printer and current print job
#
#-------------------
# Requirements
#
# - Needs Get-Stringhash Powershell module, written by Spiceworks community member cduff.
# - Available here - https://community.spiceworks.com/scripts/show/2881-get-stringhash-gets-the-hash-of-an-string
#
#-------------------
# Parameters
#
# The following parameters must be passed to the script in the PRTG sensor configurstion settings  
#
# -APIPass MYSECRETPASSWORD -DeviceIP 192.168.100.1
#
# Replace MYSECRETPASSWORD with the API password of the printer
# Replace 192.168.100.1 with the IP address of the printer
# 
#
#-------------------
#
# Introduction
#
# This script retrieves job information from Raise3d N2 series 3d printers, using the API functionality introduced
# with printer firmware 1.1.6-rev1 and RaiseTouch 1.1.0 released in October 2018. 
# For instructions on enabling the API, see the release notes at https://www.raise3d.com/blogs/news/raisetouch-1-1-0_release-notes  
#
# Each API call to the printer must include an access token which is calculated using SHA1 & MD5 hashes of the API password and
# the current time in Unix / POSIX / Epoch format. Each API call returns a JSON list containing the data for the corresponding
# printer component (system, extruders, job, etc). Some string returns (for example job status) are mapped to numeric values that
# can be translated into statuses in PRTG, using lookups. API returns are formatted as XML which is then processed by a PRTG
# EXE/Script Advanced Sensor.
#
#-------------------
# Version History
# 
# Version  Date        Notes
# 1.0      15/11/2018  Initial Release
#
# ------------------
# (c) 2018 Simon Bell | Paessler AG
# ------------------

# script parameters
Param (
        [string]$APIPass = $null,
        [string]$DeviceIP = $null, 
        [Int]$DevicePort = 10800, 
    [int]$debuglevel = 0
)

########################################################################################
# PreCondition checks
########################################################################################
# Check for required parameters
if (-not $APIPass) {
                return @"
<prtg>
  <error>1</error>
  <text>Required parameter not specified: please provide APIPass </text>
</prtg>
"@
}

if (-not $DeviceIP) {
                return @"
<prtg>
  <error>1</error>
  <text>Required parameter not specified: please provide DeviceIP</text>
</prtg>
"@
}
#URL of the IP address of the printer
$Printer = "http://" + $DeviceIP +":" + $DevicePort 


#Load hash calculation module
Import-Module get-stringhash.ps1

#Convert current time to unix time (milliseconds)
$Epoch=[Math]::Floor([decimal](Get-Date(Get-Date).ToUniversalTime()-uformat "%s"))

#Create SHA1 Hash for the password and timestamp strings
$Hash_SHA = Get-StringHash -Strings "password=$APIPass&timestamp=$Epoch" -Algorithm "SHA1" -ToLower

#create MD5 hash for the SHA1 string
$Hash_MD5 = Get-StringHash -Strings $Hash_SHA -Algorithm "MD5" -ToLower

#Pass MD5 Hash and timestamp to the printer to retrieve a valid token
$UriToken = $Printer + "/v1/login?sign=$Hash_md5&timestamp=$Epoch"
$ResposeToken = Invoke-RestMethod -uri $UriToken -Method Get

#Request returns a JSON list
$ListToken = $ResposeToken.data

#extract the token value as a variable
$AccessToken = $ListToken.token

#Start retrieving status data from the printer

#Get Running Status
$UriRunning = $Printer + "/v1/printer/runningstatus?token=$AccessToken"
$ResponseRunning = Invoke-RestMethod -uri $UriRunning -Method Get
#Request returns a JSON list
$ListRunning = $ResponseRunning.data

#Get Curr Job Status
$UriCurrentJob = $Printer + "/v1/job/currentjob?token=$AccessToken"
$ResponseCurrentJob = Invoke-RestMethod -uri $UriCurrentJob -Method Get
#Request returns a JSON list
$ListCurrentJob = $ResponseCurrentJob.data
#Round print progress % to 2 decimal places
$PrintProgress = [Math]::Round($ListCurrentjob.print_progress,2)

#Convert Job status to numeric value so PRTG Lookups can be used  
if ($ListCurrentjob.job_status -match "stopped") {$jobstatus = "0"}
elseif ($ListCurrentjob.job_status -match "running") {$jobstatus = "1"}
elseif ($ListCurrentjob.job_status -match "paused") {$jobstatus = "2"}
elseif ($ListCurrentjob.job_status -match "completed") {$jobstatus = "3"}

#If printer is idle override job status and set to stopped
if ($ListRunning.running_status -match "idle") {$jobstatus = 0}

#If printer is idle set est job duration to 0
if ($ListCurrentJob.total_time -eq 3600000) {$jobduration = "0"}
elseif ($ListCurrentJob.total_time -lt 3600000) {$jobduration = $ListCurrentJob.total_time}

#Create results table for PRTG EXE/Script Advanced Sensor to process

Write-Host 
"<prtg>"

"<result>" 
        "<channel>1. Job Status</channel>" 
        "<value>"+ $jobstatus +"</value>"
    "</result>"

    "<result>" 
        "<channel>2. Job Progress</channel>" 
        "<value>"+ $PrintProgress +"</value>"
        "<unit>Custom</unit>"
        "<customunit>%</customunit>"
        "<float>1</float>"
        "<decimalmode>2</decimalmode>"
    "</result>"

    "<result>" 
        "<channel>5. Layer Current</channel>" 
        "<value>"+ $ListCurrentJob.printed_layer +"</value>"
    "</result>"

    "<result>" 
        "<channel>6. Layers Total</channel>" 
        "<value>"+ $ListCurrentJob.total_layer +"</value>"
    "</result>"

    "<result>" 
        "<channel>3. Job Running Time</channel>" 
        "<value>"+ $ListCurrentJob.printed_time +"</value>"
        "<unit>Custom</unit>"
        "<customunit>Secs</customunit>"
    "</result>"

    "<result>" 
        "<channel>4. Job Duration (Est)</channel>" 
        "<value>"+ $jobduration +"</value>"
        "<unit>Custom</unit>"
        "<customunit>Secs</customunit>"
    "</result>"
"</prtg>"