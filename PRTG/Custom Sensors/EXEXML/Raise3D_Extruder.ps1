﻿# ___ ___ _____ ___
#| _ \ _ \_   _/ __|
#|  _/   / | || (_ |
#|_| |_|_\ |_| \___|
#    NETWORK MONITOR
#
#-------------------
# Name             Raise3D_Extruder.ps1
# Description      This sensor will show the status of a Raise3d N2 Series 3d printer extruder
#
#-------------------
# Requirements
#
# - Needs Get-Stringhash Powershell module, written by Spiceworks community member cduff.
# - Available here - https://community.spiceworks.com/scripts/show/2881-get-stringhash-gets-the-hash-of-an-string
#
#-------------------
# Parameters
#
# The following parameters must be passed to the script in the PRTG sensor configurstion settings  
#
# -APIPass MYSECRETPASSWORD -DeviceIP 192.168.100.1 -NozzleNo #
#
# Replace MYSECRETPASSWORD with the API password of the printer
# Replace 192.168.100.1 with the IP address of the printer
# Replace # with the number of the extruder you want to monitor (1 = left, 2 = right)
#
#-------------------
#
# Introduction
#
# This script retrieves extruder information from Raise3d N2 series 3d printers, using the API functionality introduced
# with printer firmware 1.1.6-rev1 and RaiseTouch 1.1.0 released in October 2018. 
# For instructions on enabling the API, see the release notes at https://www.raise3d.com/blogs/news/raisetouch-1-1-0_release-notes  
#
# Each API call to the printer must include an access token which is calculated using SHA1 & MD5 hashes of the API password and
# the current time in Unix / POSIX / Epoch format. Each API call returns a JSON list containing the data for the corresponding
# printer component (system, extruders, job, etc). Some string returns (for example job status) are mapped to numeric values that
# can be translated into statuses in PRTG, using lookups. API returns are formatted as XML which is then processed by a PRTG
# EXE/Script Advanced Sensor.
#
#-------------------
# Version History
# 
# Version  Date        Notes
# 1.0      15/11/2018  Initial Release
#
# ------------------
# (c) 2018 Simon Bell | Paessler AG
# ------------------

# script parameters
Param (
        [string]$APIPass = $null,
        [string]$DeviceIP = $null, 
        [Int]$DevicePort = 10800, 
        [ValidateRange(1,2)] [Int]$NozzleNo = 1, 
   [int]$debuglevel = 0
)

########################################################################################
# PreCondition checks
########################################################################################
# Check for required parameters
if (-not $APIPass) {
                return @"
<prtg>
  <error>1</error>
  <text>Required parameter not specified: please provide APIPass </text>
</prtg>
"@
}

if (-not $DeviceIP) {
                return @"
<prtg>
  <error>1</error>
  <text>Required parameter not specified: please provide DeviceIP</text>
</prtg>
"@
}
#URL of the IP address of the printer
$Printer = "http://" + $DeviceIP +":" + $DevicePort 


[string[]] $ExtruderNames = "Invalid","nozzle1", "nozzle2"


#Load hash calculation module
Import-Module get-stringhash.ps1

#Convert current time to unix time (milliseconds)
$Epoch=[Math]::Floor([decimal](Get-Date(Get-Date).ToUniversalTime()-uformat "%s"))

#Create SHA1 Hash for the password and timestamp strings
$Hash_SHA = Get-StringHash -Strings "password=$APIPass&timestamp=$Epoch" -Algorithm "SHA1" -ToLower

#create MD5 hash for the SHA1 string
$Hash_MD5 = Get-StringHash -Strings $Hash_SHA -Algorithm "MD5" -ToLower

#Pass MD5 Hash and timestamp to the printer to retrieve a valid token
$UriToken = $Printer +"/v1/login?sign=$Hash_md5&timestamp=$Epoch"
$ResposeToken = Invoke-RestMethod -uri $UriToken -Method Get

#Request returns a JSON list
$ListToken = $ResposeToken.data

#extract the token value as a variable
$AccessToken = $ListToken.token

#Start retrieving status data from the printer

#Get Extruder 1 data
$UriNozzle = $Printer +"/v1/printer/" + $ExtruderNames[$NozzleNo] +"?token=$AccessToken"
$ResponseNozzle = Invoke-RestMethod -uri $UriNozzle -Method Get
#Request returns a JSON list
$ListNozzle = $ResponseNozzle.data

#Create results table for PRTG EXE/Script Advanced Sensor to process

Write-Host 
"<prtg>"
    "<result>" 
        "<channel>1. Extru "+ $NozzleNo +" Temp Curr</channel>" 
        "<value>"+ $ListNozzle.nozzle_cur_temp +"</value>"
        "<unit>Custom</unit>"
        "<customunit>&#xb0;C</customunit>"
    "</result>"

    "<result>" 
        "<channel>2. Extru "+ $NozzleNo +" Temp Tgt</channel>" 
        "<value>"+ $ListNozzle.nozzle_tar_temp +"</value>"
        "<unit>Custom</unit>"
        "<customunit>&#xb0;C</customunit>"
    "</result>"

    "<result>" 
        "<channel>3. Extru "+ $NozzleNo +" Rate Curr</channel>" 
        "<value>"+ $ListNozzle.flow_cur_rate +"</value>"
     "</result>"

     "<result>" 
        "<channel>4. Extru "+ $NozzleNo +" Rate Tgt</channel>" 
        "<value>"+ $ListNozzle.nozzle_tar_rate +"</value>"
    "</result>"

"</prtg>"