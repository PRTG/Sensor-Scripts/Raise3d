Installation Instructions.
===========================================

Preparations

This custom sensor project has a standard directory structure:
All the files in the PRTG subdirectory needs to go into the PRTG program directory
(https://kb.paessler.com/en/topic/463-how-and-where-does-prtg-store-its-data).
This means, copy the files from "prtg" to "%programfiles(x86)%\PRTG Network Monitor" and the files will be copied into the correct locations in the subdirectories.

Please ** NOTE **, the destination "%programfiles(x86)%" is a "protected path " and requires elevated privileges.
(IE if you copy across the network, the "admin user may not have the correct drive mappings).

The included PowerShell scripts make use of a separate module to calculate the hash values used to create the access tokens. The module Get-Stringhash, written by Spiceworks community member cduff, can be found here:

https://community.spiceworks.com/scripts/show/2881-get-stringhash-gets-the-hash-of-an-string

This should be installed into the PowerShell directory in the usual way.

Please make sure your system is configured to allow PRTG to execute PowerShell scripts, as described in this article:

https://kb.paessler.com/en/topic/71356-guide-for-powershell-based-custom-sensors


Instructions

Add the printer as a device in PRTG, either in an existing device group, or a new one created for the printer. A "vendor icon" (Raise3D logo) is included in the archive and can be used as the device icon in the device tree.

Add EXE/Script Advanced sensors to the new device, for each of the scripts you want to use (see README.md for details of the sensors available).

If you have a dual extruder printer, you should add the "Extruder" script twice, changing the "NozzleNo" parameter in each instance, as described below.

All of the sensors require parameters to be passed to the script in the PRTG sensor configurstion settings:  

 -APIPass MYSECRETPASSWORD -DeviceIP 192.168.100.1 -NozzleNo #

      Replace MYSECRETPASSWORD with the API password of the printer
      Replace 192.168.100.1 with the IP address of the printer
      Replace # with the number of the extruder you want to monitor (1 = left, 2 = right)

 Note - The -NozzleNo paremter is onky used by the "Extruder" and "All" scripts

 The first time PRTG polls the new device and executes the scripts, the sensors channels are created. Once this is done, you can then assign "Lookups" to certain channels

 "raise3d.camerastatus.ovl" should be assigned to:

     "System" Sensor - "9. Camera Connected" Channel
     "All" Sensor - "25. Camera Connected" Channel

"raise3d.runningstatus.ovl" should be assigned to:

     "System" Sensor - "1. Printer Status" Channel
     "All" Sensor - "1. Printer Status" Channel

"raise3d.jobstatus.ovl" should be assigned to:

      "Job" Sensor - "1. Job Status" Channel
      "All" Sensor - "14. Job Status" Channel

You can now create limits (thresholds) on any of the available channels, and assign notifications to alert the operator if the limits are exceeded.      